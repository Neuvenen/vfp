#ifndef STARTUP_H
#define STARTUP_H

#include <QObject>

namespace Ps
{
    class MainView;
    class SetupTab;

    class Startup final: public QObject
    {
        Q_OBJECT
    public:
        explicit Startup();
        void show() const;

        ~Startup();
    private:
        explicit Startup(const Startup& rhs) = delete;
        Startup& operator= (const Startup& rhs) = delete;

        SetupTab& m_setupTab;
        MainView& m_mainView;
    };
} /* namespace Ps */
#endif // STARTUP_H



