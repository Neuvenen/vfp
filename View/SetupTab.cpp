#include "SetupTab.h"
#include "ui_SetupTab.h"
#include "utils.h"

namespace Ps
{

SetupTab::SetupTab(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SetupTab)
{
    ui->setupUi(this);
}

SetupTab::~SetupTab()
{
    Utils::DestructorMsg(this);
    delete ui;
}

} /* namespace Ps */
