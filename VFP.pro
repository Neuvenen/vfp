#-------------------------------------------------
#
# Project created by QtCreator 2014-12-14T18:15:45
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = VFP
TEMPLATE = app

QMAKE_CXXFLAGS += -std=c++11

SOURCES += main.cpp\
    View/mainview.cpp \
    startup.cpp \
    utils.cpp \
    View/SetupTab.cpp \
    Model/settings.cpp

HEADERS  += View/mainview.h \
    startup.h \
    utils.h \
    View/SetupTab.h \
    Model/settings.h

FORMS    += View/mainview.ui \
    View/SetupTab.ui

RESOURCES += \
    VfpREsources.qrc
