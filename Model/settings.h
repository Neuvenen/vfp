#ifndef SETTINGS_H
#define SETTINGS_H

#include <QObject>

class QString;
class QStringListModel;
class QJsonParseError;
class QJsonObject;
class QFile;
class QDir;

namespace Ps
{

    class Settings : public QObject
    {
        Q_OBJECT
    public:
        explicit Settings(QObject *parent, QString filename);

    signals:
        void NotifyStatusMessage(QString message);
    private:
        explicit Settings(const Settings& rhs) = delete;
        Settings& operator= (const Settings& rhs) = delete;

        QString ReadJsonFile();
        QString ReadJsonFromInternalResource();

        QString m_filename;
        void SendErrorMessage(const QString &msg);
    };
} /* namespace Ps */
#endif // SETTINGS_H



